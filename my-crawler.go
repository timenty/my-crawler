package main

import (
	"encoding/json"
	"fmt"
	"github.com/gocolly/colly"
	"log"
	"net/http"
	"strings"
	"time"
)
// Почти рабочая версия
type PageInfo struct {
	Page    string    `json:"page"`
	Status	uint	`json:"status"`
	Deep int		`json:"deep"`
	LastUrl string `json:"last_url"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	URL := r.URL.Query().Get("url")
	domain := strings.Split(strings.Split(URL, "://")[1],"/")[0]
	countParsedPages := 0
	listed := []PageInfo{}
	// Info
	if URL == "" {
		log.Println("missing URL argument")
		return
	}
	log.Println("Domain", domain)
	log.Println("Start url", URL)
	// Collector settings
	c := colly.NewCollector(
		colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36"),
		colly.AllowedDomains(domain),
		colly.Async(true),
		colly.MaxDepth(21),
	)
	// Limit rules
	_ = c.Limit(&colly.LimitRule{
		DomainGlob:  "*" + strings.Split(domain, ".")[0] + ".*",
		Parallelism: 4,
		Delay:       140 * time.Millisecond, // самый разумный результат таков, нужно будет поколдовать, что бы сделать ещё меьше задержку
		RandomDelay: 85 * time.Millisecond,
	})
	// On every request
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String()) // выводим посещённую ссылку
	})
	// Error handler
	c.OnError(func(r *colly.Response, e error) {
		newPage := PageInfo{
			Page:     r.Request.URL.String(),
			Status:   uint(r.StatusCode),
			Deep: r.Request.Depth,
		}
		listed = append(listed, newPage)
		fmt.Println("status", r.StatusCode)
	})

	c.OnResponse(func(r *colly.Response) {
		last_url := r.Ctx.Get("last_url")
		newPage := PageInfo{
			Page:     r.Request.URL.String(),
			Status:   uint(r.StatusCode),
			Deep: r.Request.Depth,
			LastUrl: last_url,
		}
		listed = append(listed, newPage)
		countParsedPages++
		fmt.Println("countParsedPages", countParsedPages)
	})
	// On every a element which has href attribute call callback
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if link == "" { return }
		/*
			оптимальные значения вычисленные эксперементальным путём для задержки это
			40ms - для особо привередливых, 50ms - супер безопасная задержка, 10ms оптимум для сайтов без защиты от DDOS
		 */
		time.Sleep(40 * time.Millisecond)
		_ = e.Request.Visit(e.Request.AbsoluteURL(link))
	})
	// Start scraping
	fmt.Println("visit Url", URL)
	_ = c.Visit(URL)
	c.Wait()
	// dump results
	b, err := json.Marshal(listed)
	if err != nil {
		log.Println("failed to serialize response:", err)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(b)
}

func main() {
	// example usage: curl -s 'http://127.0.0.1:7171/?url=http://go-colly.org/'
	addr := ":7171"

	http.HandleFunc("/", handler)

	log.Println("listening on", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}